# Orbscura

A prototype dark web microblogging system using Tor v3 onion services and the new rust Tapir/Cwtch library. It is mostly
intended as a testbed for [tapir-cwtch](https://crates.io/crates/tapir-cwtch) but also serves as a complete example
application for prototyping metadata resistant applications.

## Building

    cargo run --release
    
You will need to have a `tor` binary in your path.

## Usage & Screenshots

### Compose new Orbs

Press `e` and then `<Return>` when finished to post a new Orb.

![](./screenshots/compose.png)

### Rebroadcasting

Use the arrow keys to select an Orb, then press `r` to Rebroadcast on your own feed.

![](./screenshots/rebroadcast.png)

![](./screenshots/select.png)


### Following new Orbs

Press `c` to copy your Orb address to give to other people. With an Orb address in your clipboard you can press `f`
to follow the address:

![](./screenshots/share.png)

![](./screenshots/follow.png)



